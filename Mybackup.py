import os
import shutil
import hashlib
import pickle
import Logging
import re

BACKUPARCHIEVE = raw_input("Please type the path of directory BACKUPARCHIEVE with the '/' or '\' to check if does it exists:\n")
myArchieve = raw_input("Please input the path of myArchieve directory '/' or '\' as BACKUPARCHIEVE does not exist:\n")
src = raw_input("please input the directory you want to backup for saving:\n")


def    GetHash (filename):
#==========================================================================
    """CreateFileHash (file): create a signature for the specified file
       Returns a tuple containing  values:
          (the pathname of the file, SHA1 hash)
    """

    f = None
    signature = None
    try:

        f = open(filename, "rb")  # open for reading in binary mode
        hash = hashlib.sha1()
        s = f.read(16384)
        while (s):
            hash.update(s)
            s = f.read(16384)

        hashValue = hash.hexdigest()   
        signature = hashValue    
    except IOError:
        signature = None
    except OSError:
        signature = None
    finally:
        if f:
            f.close()
        return signature


    

def Dictionary():
    if not os.path.exists(os.path.join(myArchieve,'index.pkl')): 
        myDict = {}
    else:
        myDict = pickle.load(open(myArchieve+os.sep+'index.pkl', 'rb'))
        
    for src_file in os.listdir(src):
        full_path1 = os.path.join(src,src_file)
        hashdigit = GetHash(full_path1)
        myDict[full_path1] = hashdigit
        print myDict       
    pickle.dump(myDict,open(myArchieve+os.sep+'index.pkl', 'wb'))



def save(src):
    print "this part is for saving which includes save, rename and store to dictionary."
    for src_file in os.listdir(src):
        full_path1 = os.path.join(src, src_file)
        hexh1 = GetHash(full_path1)
        
        if os.listdir(myArchieve+os.sep+"objects") == []:
            print "following copy"+full_path1
            shutil.copy(full_path1, myArchieve+os.sep+"objects")
            print "renaming"+full_path1
            os.rename(os.path.join(myArchieve+os.sep+"objects",src_file),os.path.join(myArchieve+os.sep+"objects",hexh1))
        else:
            flag=False
            for des_file in os.listdir(myArchieve+os.sep+"objects"):
                full_path2 = os.path.join(myArchieve+os.sep+"objects", des_file)
                hexh2 = GetHash(full_path2)
                if hexh2 == hexh1:                
                    print "did this before"+full_path1
                    flag=True
                    break
            if flag==False:
                print "following copy"+full_path1
                shutil.copy(full_path1, myArchieve+os.sep+"objects")
                print "renaming"+full_path1
                os.rename(os.path.join(myArchieve+os.sep+"objects",src_file),os.path.join(myArchieve+os.sep+"objects",hexh1))
    Dictionary();
       

def check():
    print "this part for checking"
    if os.listdir(myArchieve+os.sep+"objects") == []:
        print "no backuped files have been stored."
    else:
        i = 0
        for des_file in os.listdir(myArchieve+os.sep+"objects"):
            full_path2 = os.path.join(myArchieve+os.sep+"objects", des_file)
            hexh2 = GetHash(full_path2)
            if hexh2 != des_file:
                print full_path2,"has not been stored."               
            else:
                myDict = pickle.load(open(myArchieve+os.sep+'index.pkl', 'rb'))
                match = False
                for value in myDict.values():
                    if value == des_file:
                        print full_path2,"has been stored"
                        i += 1
                        match = True
                        break
                    else:
                        pass
                if match == False:
                        print full_path2+"has not been stored."
    print i,
    print "is the Number of the correct entries are "
     

  
def list():
    pattern = raw_input("Please input the pattern you want to match:\n") 
    print "this part is for listing."
    srcdir = os.listdir(src)
    if pattern != "":
        for eachline in srcdir:
            if pattern in eachline:
                full_path_eachline = os.path.join(src,eachline)
                print full_path_eachline

    else:
        for eachline in srcdir:
            full_path_eachline = os.path.join(src,eachline)
            print full_path_eachline


def restore():
    recover = raw_input("please input the directory you want to restore\n")
    
    print "please run restore again and Make sure your input without an os.sep."

    txt = raw_input("please input the keywords of the filename\n")
    
    myDict = pickle.load(open(myArchieve+os.sep+'index.pkl', 'rb'))
    for k , v in myDict.iteritems():
        if re.search(txt, k):
            getpath = k
            getvalue = v
            splitpath = getpath.split(':')
            a=splitpath[1]
            b = a.replace(os.sep,'-')
            print b
            flag1 = False       
            for cover_file in os.listdir(recover):
                if cover_file == b:
                    flag1 = True
                    print "restore",b,"before"
            if flag1 ==False: 
                flag2=False
                for des_file in os.listdir(myArchieve+os.sep+"objects"):
                    full_path2 = os.path.join(myArchieve+os.sep+"objects", des_file)
                    hexh2 = GetHash(full_path2)
                    if hexh2 == getvalue:
                        shutil.copy(full_path2,recover)
                        os.rename(os.path.join(recover,des_file), os.path.join(recover,b))
                        flag2=True
                        break
                if flag2 == False:
                    print "can not find filed match your input"  
        else:
            print "file has not be stored."

try:    
    if(os.path.exists(BACKUPARCHIEVE)):
        os.mkdir(BACKUPARCHIEVE+os.sep+"objects")
        print "BACKUPARCHIEVE exists."
    else:
        if(not(os.path.exists(myArchieve))):
            
            print "BACKUPARCHIEVE Doesn't exists, creating myArchieve"
        
            os.mkdir(myArchieve)
            os.mkdir(myArchieve+os.sep+"objects")

except:
    OSError
    
print "please input the command you want to do, includes(save, check, list, restore)"

command = raw_input("Enter command:  ")

if command == 'save':
    save(src)
    
elif command == 'check':            
    check()
    
elif command == 'list':
    list()
    
elif command == 'restore':
    restore()